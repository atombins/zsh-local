INSTALL_DIR?=${HOME}/.zsh-local/
VERSION?=5.9
SOURCE_URL?=https://sourceforge.net/projects/zsh/files/zsh/${VERSION}/zsh-${VERSION}.tar.xz/download

PATH:=${INSTALL_DIR}/ncurses/bin:${PATH}
LD_LIBRARY_PATH:=${INSTALL_DIR}/ncurses/lib:${LD_LIBRARY_PATH}
CFLAGS:=-I${INSTALL_DIR}/ncurses/include
CPPFLAGS:=-I${INSTALL_DIR}/ncurses/include
LDFLAGS:="-L${INSTALL_DIR}/ncurses/lib"

.PHONY: download build install ncurses

install: ${INSTALL_DIR}/bin/zsh-local
download: zsh-${VERSION}.tar.xz
build: zsh-${VERSION}
uninstall:
	rm -rf ${INSTALL_DIR}
ncurses: ${INSTALL_DIR}/ncurses

${INSTALL_DIR}/ncurses:
	wget https://invisible-island.net/archives/ncurses/ncurses-6.4.tar.gz
	tar -zxvf ncurses-6.4.tar.gz
	cd ncurses-6.4 && CXXFLAGS=' -fPIC' CFLAGS=' -fPIC' ./configure --prefix=${INSTALL_DIR}/ncurses --enable-shared
	cd ncurses-6.4 && make
	cd ncurses-6.4/progs &&	./capconvert
	cd ncurses-6.4 && make install


${INSTALL_DIR}/bin/zsh-local: zsh-${VERSION}
	cd $< && make install
	ln -s zsh-local ${INSTALL_DIR}/bin/zsh

zsh-${VERSION}: zsh-${VERSION}.tar.xz ${INSTALL_DIR}/ncurses
	tar xvf zsh-${VERSION}.tar.xz
	cd $@ && PATH=${PATH} LD_LIBRARY_PATH=${LD_LIBRARY_PATH} CFLAGS="${CFLAGS}" CPPFLAGS="${CPPFLAGS}" LDFLAGS="${LDFLAGS}" ./configure --prefix=${INSTALL_DIR} --program-suffix=-local
	cd $@ && make

zsh-${VERSION}.tar.xz:
	wget -O zsh-${VERSION}.tar.xz ${SOURCE_URL}
